import { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { CssBaseline, ThemeProvider } from '@mui/material'; 
import Home from "./pages/Home";
import Results from "./pages/Results";
import Theme, {DarkTheme} from "./components/Theme";
import UseSearchContext from './hooks/UseSearchContext';

const App = () => {
    const { darkMode, paths, navigate, pathName } = UseSearchContext();

    useEffect(() => {
        if (pathName === "/" ) {
            navigate(paths.homepage);
        }
    }, [navigate, paths.homepage, pathName]);

    return (
        <ThemeProvider theme={{ ...(darkMode? DarkTheme : Theme) }}>
            <CssBaseline />
            <Routes>
                <Route path={paths.homepage} element={<Home />} />
                <Route path={paths.search} element={<Results />} />
                <Route path={paths.images} element={<Results />} />
                <Route path={paths.videos} element={<Results />} />
            </Routes>
        </ThemeProvider>
    );
};
export default App;
