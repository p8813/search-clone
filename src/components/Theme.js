import { createTheme } from "@mui/material";

const primaryMain = "#00b700";
const secondaryMain = "#ffffff";

const Theme = createTheme({
    palette: {
        mode: "light",
        primary: {
            main: primaryMain
        },
        secondary: {
            main: secondaryMain
        },
        text: {
            primary: "#000000",
            secondary: "#6a6a6a"
        }
    }
});

const DarkTheme = createTheme({
    palette: {
        mode: "dark",
        primary: {
            main: primaryMain
        },
        secondary: {
            main: secondaryMain
        },
        text: {
            primary: "#ffffffff",
            secondary: "#efefef"
        }
    }
})

export { DarkTheme };
export default Theme;
