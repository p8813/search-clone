import { FormGroup, FormControlLabel, Switch, Tooltip } from "@mui/material";
import DarkModeIcon from '@mui/icons-material/DarkMode';
import ApiIcon from '@mui/icons-material/Api';
import UseSearchContext from "../hooks/UseSearchContext";

const ToggleSwitches = () => {
    const { toggleOnApi, onApi, toggleDarkMode, darkMode } = UseSearchContext(); 

    return (
        <FormGroup sx={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
            <Tooltip title="Dark Mode">
                <FormControlLabel control={<Switch checked={darkMode} onChange={toggleDarkMode}/>} label={<DarkModeIcon />} />
            </Tooltip>
            <Tooltip title="API Switch">
                <FormControlLabel control={<Switch checked={onApi} onChange={toggleOnApi}/>} label={<ApiIcon />} />
            </Tooltip>
        </FormGroup>
    );
};

export default ToggleSwitches;
