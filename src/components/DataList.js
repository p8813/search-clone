import UseSearchContext from "../hooks/UseSearchContext";
import { Box, Stack, Grid, Typography, Link, ImageList, ImageListItem, ImageListItemBar } from "@mui/material";
import ReactPlayer from "react-player/lazy";

const DataList = () => {
    const { paths, data, pathName } = UseSearchContext();
    
    console.log("Data:", data);
    let renderedData = null;
    if(data){
        if (pathName === paths.search) {
            renderedData = <Stack direction="column" gap={1}>
            {
                data?.webPages?.value?.map(({id, name, url, displayUrl, snippet}) => {
                    return (
                        <Box key={id}>
                            <Box>
                                <Typography variant="body1">{name}</Typography>
                            </Box>
                            <Box>
                                <Link href={url} target="_blank" rel="noopener noreferrer">{displayUrl}</Link>
                            </Box>
                            <Box>
                                <Typography variant="body2">{snippet}</Typography>
                            </Box>
                        </Box>
                    );
                })
            }
            </Stack>
        } else if (pathName === paths.images) {
            renderedData = <Box>
                { data?.value?.length ? (
                    <ImageList cols={3} gap={12}>
                    {
                        data?.value?.map(({imageId, name, thumbnailUrl, hostPageUrl, hostPageDisplayUrl, thumbnail}) => (
                                <ImageListItem component={Link} key={imageId} href={hostPageUrl} target="_blank" rel="noopener noreferrer">
                                    {  thumbnail?.width && thumbnail?.height && (
                                        <img
                                            src={thumbnailUrl}
                                            alt={name}
                                            loading="lazy"
                                            width={thumbnail.width}
                                            height={thumbnail.height}
                                        />
                                    )}
                                    <ImageListItemBar title={name} subtitle={hostPageDisplayUrl} />
                                </ImageListItem>
                        ))
                    }
                    </ImageList>
                ): null}
            </Box>
        } else if (pathName === paths.videos) {
            renderedData = <Grid container >
                { data.value?.length ? (
                        data.value?.map(({ videoId, name, publisher, contentUrl, embedHtml, thumbnail, thumbnailUrl}) => {
                            let content = null;
                            if(publisher) {
                                if (publisher[0].name.toLowerCase() === "youtube") {
                                    content = <ReactPlayer url={contentUrl} width={thumbnail.width} height={thumbnail.height} />
                                } else if (embedHtml) {
                                    content = <Box dangerouslySetInnerHTML={{__html: embedHtml}}/>
                                } else {
                                    content = (
                                        <Box component={Link} href={contentUrl} target="_blank" rel="noopener noreferrer" >
                                            <img
                                                src={thumbnailUrl}
                                                alt={name}
                                                loading="lazy"
                                                width={thumbnail.width}
                                                height={thumbnail.height}
                                            />
                                        </Box>
                                    );
                                };
                            };
                            return (
                                <Grid key={videoId} item sm={12} md={6}>
                                    {content}
                                </Grid>
                            )
                        })
                ) : null}
            </Grid>
        }
    };
    console.log("renderedData:", renderedData)
    return (
        <Box>{renderedData}</Box>
    );
};

export default DataList;
