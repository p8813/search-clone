import { useState } from "react";
import { TextField, Button, Box } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import UseSearchContext from "../hooks/UseSearchContext";

const SearchBar = () => {
    const [searchText, setSearchText] = useState("");

    const {onSubmit, pathName, navigate, paths } = UseSearchContext();


    const buttonStyle = {
        bgcolor: "secondary.main",
        width: pathName === paths.homepage ? "200px": "10px",
        maxWidth: "100%",
        pt: 1.5,
        pb: 1.5,
        textTransform: "none",
        fontSize: 17
    };

    const formBoxStyle = {
        display: "flex",
        flexDirection: pathName === paths.homepage ? "column": "row",
        alignItems: "center",
        gap: 1.5
    };

    const handleChange = (event) => {
        setSearchText(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (searchText) { 
            onSubmit(searchText);
            if (pathName === paths.homepage) {
                navigate(paths.search);
            };
        };
    };

    const buttonBody = pathName === paths.homepage ? "Search" : <SearchIcon /> ;

    return (
        <form onSubmit={handleSubmit}>
            <Box sx={formBoxStyle}>
                <TextField value={searchText} onChange={handleChange} label="Search" variant="outlined" sx={{ width: "100%" }} />
                <Button variant="outlined" type="submit" sx={buttonStyle}>{buttonBody}</Button>
            </Box>
        </form>
    );  
};

export default SearchBar;
