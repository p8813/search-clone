import { useContext } from "react";
import SearchContext from "../context/SearchContext";

const UseSearchContext = () => {
    return useContext(SearchContext);
};

export default UseSearchContext;
