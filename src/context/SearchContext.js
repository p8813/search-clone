import { createContext, useState, useEffect, useCallback } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import webData from "../api_responses/web.json";
import imageData from "../api_responses/images.json";
import videoData from "../api_responses/videos.json"; 
import axios from "axios";

const SearchContext = createContext();

const SearchProvider = ({ children }) => {
    const [searchTerm, setSearchTerm] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState(null);
    const [onApi, setOnApi] = useState(false);
    const [darkMode, setDarkMode] = useState(false);

    const location = useLocation();
    const pathName = location.pathname;
    const navigate = useNavigate();
    const mainTitle = "Search";
    const subTitle = "Clone";
    const paths = {
        homepage: "/search-clone/",
        search: "/search-clone/search",
        images: "/search-clone/images/search",
        videos: "/search-clone/videos/search"
    }

    console.log("searchTerm:", searchTerm);
    console.log("pathName:", pathName);
    console.log("onApi:", onApi);
    console.log("darkMode:", darkMode);

    const baseURL = "https://api.bing.microsoft.com/v7.0";

    useEffect(() => {
        const storedSearchTerm = localStorage.getItem('searchTerm');
        if (storedSearchTerm) {
            setSearchTerm(storedSearchTerm);
        };
    }, []);

    const onSubmit = (newSearchTerm) => {
        setData(null);
        localStorage.setItem('searchTerm', newSearchTerm);
        setSearchTerm(newSearchTerm);
    };

    const toggleOnApi = () => {
        setOnApi(!onApi);
    };

    const toggleDarkMode = () => {
        setDarkMode(!darkMode);
    };

    const getData = useCallback(async (term) => {
        console.log("getting called");
        if (pathName !== paths.homepage) {
            console.log("getting called inside");
            setIsLoading(true);
            const options = {
                method: "GET",
                url: `${baseURL}${pathName}`,
                params: {
                    q: term,
                    count: 20
                },
                headers: {
                    "Ocp-Apim-Subscription-Key": process.env.REACT_APP_OCP_APIM_SUBSCRIPTION_KEY
                }
            };

            try {
                let results = null;
                if (onApi) {
                    const response = await axios.request(options);
                    results = response.data;
                } else {
                    switch (pathName) {
                        case paths.images:
                            results = imageData;
                            break;
                        case paths.videos:
                            results = videoData;
                            break;
                        default:
                            results = webData;
                            break;
                    };
                };
                setIsLoading(false);
                return results;
            } 
            catch (error) {
                console.log("Error:", error);
                setIsLoading(false);
                return null;
            };
        };
    }, [pathName, onApi, paths.homepage, paths.images, paths.videos]);

    useEffect(() => {
        const fetchData = async () => {
            const newData = await getData(searchTerm);
            setData(newData);
        };
        fetchData();
    }, [searchTerm, getData]);

    const values = {
        paths: paths,
        data: data,
        onSubmit: onSubmit,
        pathName: pathName,
        isLoading: isLoading,
        navigate: navigate,
        mainTitle: mainTitle,
        subTitle: subTitle,
        toggleOnApi: toggleOnApi,
        onApi: onApi,
        toggleDarkMode: toggleDarkMode,
        darkMode: darkMode
    };

    return (
        <SearchContext.Provider value={values}>
            {children}
        </SearchContext.Provider>
    ); 
};

export { SearchProvider };
export default SearchContext;
