import { Container, Stack, Box, Typography } from '@mui/material';
import UseSearchContext from '../hooks/UseSearchContext';
import SearchBar from '../components/SearchBar';
import ToggleSwitches from '../components/ToggleSwitches';

const Home = () => {
    const { mainTitle, subTitle } = UseSearchContext();

    return (
        <Container >
            <Box sx={{ position: "relative" }}>
                <ToggleSwitches />
            </Box>
            <Container sx={{ mt: 22 }}>
                <Stack direction="column" sx={{ alignItems: "center"}} gap={4} >
                    <Box sx={{ display: "flex"}}>
                        <Typography variant='h1' sx={{ fontSize:{xs: 60, sm: 84}, fontWeight: "bold", color: "primary.main" }}>
                            {mainTitle}
                        </Typography>
                        <Typography variant='caption' sx={{ fontSize: 24, fontStyle: "italic", pt:2, color: "text.secondary" }} >
                            {subTitle}
                        </Typography>
                    </Box>
                    <Box sx={{ width: 700, maxWidth: "100%" }}>
                        <SearchBar />
                    </Box>
                </Stack>
            </Container>
        </Container>
    );
}

export default Home;
