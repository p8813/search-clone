import { useState, useEffect } from "react";
import { Link as RouterLink } from "react-router-dom";
import { Container, Box, Stack, CircularProgress, Button, Typography, Tabs, Tab } from "@mui/material";
import UseSearchContext from "../hooks/UseSearchContext";
import SearchBar from "../components/SearchBar";
import DataList from "../components/DataList";
import ToggleSwitches from "../components/ToggleSwitches";

const Results = () => {
    const [tabValue, setTabValue] = useState(0);
    const {paths, onSubmit, pathName, isLoading, navigate, mainTitle, subTitle} = UseSearchContext();

    const tabStyle = {
        textTransform:"none"
    };

    const handleLogoClick = () => {
        onSubmit("");
        navigate(paths.homepage);
    };

    const handleTabChange = (event, newValue) => {
        setTabValue(newValue);
    };

    useEffect(()=> {
        switch(pathName) {
            case paths.images:
                setTabValue(1);
                break;
            case paths.videos:
                setTabValue(2);
                break;
            default:
                setTabValue(0);
                break;
        };
    }, [pathName, paths.images, paths.videos]);

    return (
        <Container>
            <Box sx={{display: "flex", flexDirection: "column", justifyContent: "center", mt: 2, mb: 4}}>
                <ToggleSwitches />
                <Box sx={{ display: "flex", justifyContent: "center" }}>
                    <Button onClick={handleLogoClick} sx={{ textTransform: "none", maxWidth: 300 }} >
                        <Typography variant="h4">
                            {mainTitle}
                        </Typography>
                        <Typography variant='caption' sx={{ fontSize: 15, fontStyle: "italic", color: "text.secondary" }}>
                            {subTitle}
                        </Typography>
                    </Button>
                </Box>
            </Box>
            <Stack direction="column" gap={2}>
                <SearchBar />
                <Box>
                    <Tabs value={tabValue} onChange={handleTabChange} >
                        <Tab sx={tabStyle} label="All" component={RouterLink} to={paths.search} />
                        <Tab sx={tabStyle} label="Images" component={RouterLink} to={paths.images} />
                        <Tab sx={tabStyle} label="Videos" component={RouterLink} to={paths.videos} />
                    </Tabs>
                </Box>
                <DataList />
                <Box sx={{display: "flex", justifyContent:"center", mt:18}}>
                    {isLoading && <CircularProgress size={140} thickness={1}/>}
                </Box>
            </Stack>
        </Container>
    );
};

export default Results;
